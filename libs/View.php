<?php

class View
{
	private $fileName;

	public function __construct($fileName)
	{       
		  $this->fileName = $fileName;

	}

    public function showTpl($data)
    {
        $file = HOST . DS . TEMPLATE_DIR . DS . $this->fileName . '.php';
        if (is_file($file))
        {
            ob_start();
            extract($data, EXTR_SKIP);
            include($file);
            return ob_get_clean();
        }

        return false;

    }
}
