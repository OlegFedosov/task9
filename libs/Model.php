<?php 

class Model
{ 

    public function setMessage($message, $type = 1)
    {
        $_SESSION['GLOBAL_MESSAGE']['text'] = $message;
        $_SESSION['GLOBAL_MESSAGE']['type'] = in_array($type, [1, 2]) ? $type : 1;

    }

    public function getMessage() 
    {
        if (isset($_SESSION['GLOBAL_MESSAGE']['text']))
        {
            $mes = $_SESSION['GLOBAL_MESSAGE'];
            unset($_SESSION['GLOBAL_MESSAGE']);
            return $mes;
        }
        return false;
    }

}
