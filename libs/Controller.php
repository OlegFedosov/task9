<?php

class Controller
{
    private $model;
    private $view;
    private $content;
    private $title;
    private $data;
    private $data2;
    private $data3;
    private $data4;

    public function __construct()
    {
        $this->model = new Model();
        $this->view = new View(TEMPLATE_MAIN);
        $this->title = 'HTML helper';

        $this->pageDefault();
    }

    private function pageDefault()
    {
        //use select
        $this->filler();
        $select = HtmlHelper::select(
            [
                'class' => 'form-control',
                'required' => true,
                'name' => 'name',
                'size' => 4,
            ], $this->data, 3);

        //use radio groups
        $radio = HtmlHelper::radioGroup('name', $this->data, 2);

        //use list
        $list = HtmlHelper::itemsList(
            [
                //'class' => 'list-unstyled',
                'numeric' => false,
            ], $this->data2);

        //use description
        $description = HtmlHelper::descriptionLists(
            [
               'class' => 'dl-horizontal',
            ], $this->data3);

        //use table
        $table = HtmlHelper::makeTable(
            [
                'class' => 'table table-bordered',
                'th' => true,
                'align' => 'center',
            ], $this->data4);

        $view = new View('forms');
        $this->content = $view->showTpl(compact('select', 'radio', 'list', 'description', 'table'));
    }

    private function filler() {

        $this->data = ['Ivan', 'Andrew', 'Petya', 'Vasya', 'Igor', 'Slava'];
        $this->data2 = ['Ivan', 'Andrew', 'Petya', 'Vasya' => [1, 2, 3, 4, 5, 6 => ['some', 'list', 'here'], 7, 8, 9], 'Igor', 'Slava'];
        $this->data3 = ['some description' => 'some test text, more some text', 'some description2' => 'some test text, more some text2'];
        $this->data4 = [
            ['№', 'text1', 'text2', 'text3', 'text4'],
            ['1', 'some test text, more some text1', 'some description1', 'some test text1', 'more some text1'],
            ['2', 'some test text, more some text2', 'some description2', 'some test text2', 'more some text2'],
            ['3', 'some test text, more some text3', 'some description3', 'some test text3', 'more some text3'],
            ['4', 'some test text, more some text4', 'some description4', 'some test text4', 'more some text4']
        ];

    }

    public function render()
    {
        $content = $this->content;
        $message = $this->model->getMessage();
        $title = $this->title;
        return $this->view->showTpl(compact('content', 'message', 'title'));
    }
}
