<?php

Class HtmlHelper
{
    /**
     * @param array $attr
     * @param array $data
     * @param bool $selected
     * @return bool|string
     */
    public static function select(array $attr = [], array $data = [], $selected = false)
    {
       if (!count($data))
       {
           return false;
       }
       
       $string = '<select ';
       $string .= $attr['mult'] ? 'multiple ' : '';
       $string .= $attr['required'] ? 'required ' : '';
       $string .= $attr['disabled'] ? 'disabled ' : '';
       $string .= $attr['class'] ? 'class="' . $attr['class'] . '" ' : '';
       $string .= $attr['size'] ? 'size="' . $attr['size'] . '" ' : '';
       $string .= $attr['name'] ? 'name="' . $attr['name'] . '" ' : '';
       $string .= '>';
       
       foreach ($data as $k => $v)
       { 
           $string .= '<option value=" ' . $k . '" ' . ($k == $selected ? 'selected' : null) . '>' . $v . '</option>';
       }
       $string .= '</select>';
       return $string;   
    }

    /**
     * @param $name
     * @param array $data
     * @param bool $checked
     * @param string $display
     * @return bool|string
     */
    public static function radioGroup($name, array $data = [], $checked = false, $display = 'block')
    {
       if (!count($data))
       {
           return false;
       }

       $string = '';

       foreach ($data as $k => $v)
       {
           if ($display == 'block')
           {
               $string .= '<div class="radio">';
           }

           $string .= '<label' . ($display == 'inline' ? 'class="radio-inline"' : null) . '>';
           $string .= '<input type="radio" value="' . $k . '" ' . ($k == $checked ? 'checked ' : null);
           $string .= 'name= "' . $name . '"/>';
           $string .= $v;
           $string .= '</label>';

           if ($display == 'block')
           {
               $string .= '</div>';
           }
       }
       
       return $string;   
    }

    /**
     * @param array $attr
     * @param array $data
     * @return bool|string
     */
    public static function itemsList(array $attr = [], array $data = [])
    {
        if (!count($data))
        {
            return false;
        }

        $tag = $attr['numeric'] ? 'ol' : 'ul';

        $string = '<' . $tag;
        $string .= $attr['class'] ? ' class="' . $attr['class'] . '" ' : '';
        $string .= '>';
        foreach ($data as $k => $v)
        {
            if (is_array($v))
            {
                $string .= '<li>' . $k;
                $string .= static::itemsList($attr, $v);
                $string .= '</li>';
            }
            else
            {
                $string .= '<li>' . $v .'</li>';
            }

        }

        $string .= '</' . $tag . '>';
        return $string;
    }

    /**
     * @param array $attr
     * @param array $data
     * @return bool|string
     */
    public static function descriptionLists(array $attr = [], array $data = [])
    {
        if (!count($data))
        {
            return false;
        }

        $string = '<dl';
        $string .= $attr['class'] ? ' class="' . $attr['class'] . '" ' : '';
        $string .= '>';
        foreach ($data as $k => $v)
        {
            $string .= '<dt>' . $k .'</dt>';
            $string .= '<dd>' . $v .'</dd>';
        }

        $string .= '</dl>';
        return $string;
    }

    /**
     * @param array $attr
     * @param array $data
     * @return bool|string
     */
    public static function makeTable(array $attr = [], array $data = [])
    {
        if (!count($data))
        {
            return false;
        }

        $i = 0;
        $string = '<table ';
        $string .= $attr['class'] ? ' class="' . $attr['class'] . '" ' : '';
        $string .= '>';

        foreach ($data as $tr)
        {
            $i++;
            $string .= '<tr>';
            foreach ($tr as $td)
            {
                if (1 == $i and $attr['th'])
                {
                    $string .= '<th ' . ($attr['align'] ? 'style="text-align: ' . $attr['align'] . ';" ' : null) . '>'. $td .'</th>';
                }
                else
                {
                    $string .= '<td ' . ($attr['align'] ? 'align=' . $attr['align'] . ' ' : null) . '>'. $td .'</td>';
                }
            }
            $string .= '</tr>';
        }

        $string .= '</table>';

        return $string;
    }
    
}           
               
           
           
  
        
